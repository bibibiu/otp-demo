package com.bibibiu.demo.services;

import com.bibibiu.demo.domain.Student;

/**
 * created 3/3/19 12:54 AM
 *
 * @author arthur
 */
public interface StudentService extends GenericService<Student,Long>{
    Student findByRegistrationNumber(String regNum);
}
