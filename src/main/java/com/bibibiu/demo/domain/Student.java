package com.bibibiu.demo.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * created 3/3/19 12:19 AM
 *
 * @author arthur
 */
@Data
@Entity
@Table(name = "students")
public class Student implements Serializable {
    private static final long serialVersionUID = 4L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String registrationNumber;
    private String firstName;
    private String lastName;
}
