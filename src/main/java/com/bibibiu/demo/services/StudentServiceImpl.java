package com.bibibiu.demo.services;

import com.bibibiu.demo.domain.Student;
import com.bibibiu.demo.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * created 3/3/19 12:56 AM
 *
 * @author arthur
 */
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Service
@Transactional
public class StudentServiceImpl extends GenericServiceImpl<Student,Long> implements StudentService {

    @Autowired
    private  StudentRepository studentRepository;

    @Override
    public Boolean contains(Student object) {
        Optional<Student> optionalStudent = studentRepository.findByRegistrationNumber(object.getRegistrationNumber());
        return optionalStudent.isPresent();
    }


    @Override
    public Student findByRegistrationNumber(String regNum) {
        return studentRepository.findByRegistrationNumber(regNum).orElseThrow(()->new RuntimeException("Not found"));
    }

}
