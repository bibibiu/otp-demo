package com.bibibiu.demo.services;

import com.bibibiu.demo.domain.Model.MyUserModel;
import com.bibibiu.demo.domain.MyUser;

/**
 * created 3/3/19 7:29 PM
 *
 * @author arthur
 */
public interface MyUserService extends GenericService<MyUser,Long> {
    MyUserModel save(MyUserModel userModel);
    Boolean contains(MyUserModel userModel);
    MyUser findByUsername(String username);
}
