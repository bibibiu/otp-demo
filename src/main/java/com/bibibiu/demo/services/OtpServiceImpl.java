package com.bibibiu.demo.services;

import com.bibibiu.demo.domain.Model.MyUserModel;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * created 3/4/19 12:31 AM
 *
 * @author arthur
 */
@Slf4j
@Service
public class OtpServiceImpl implements OtpService {


    private LoadingCache<String,Integer> otpCache;

    @Value("${otp.min.value:100000}")
    private Integer OTP_MIN_VALUE;

    public OtpServiceImpl(@Value("${otp.expiration.time:1}")Integer EXPIRES_MINS) {
        super();
        log.debug("OTP expiration time: "+ EXPIRES_MINS);
        otpCache = CacheBuilder.newBuilder()
                .expireAfterWrite(EXPIRES_MINS, TimeUnit.MINUTES)
                .build(new CacheLoader<String, Integer>() {
                    @Override
                    public Integer load(String s) throws Exception {
                        return 0;
                    }
                });
    }

    @Override
    public Integer generateOtp(String username) {
        Random random = new Random();
        int otp = OTP_MIN_VALUE + random.nextInt(900000);
        otpCache.put(username, otp);
        return otp;
    }

    @Override
    public Integer getOtp(String username) {
        try {
            return otpCache.get(username);
        } catch (ExecutionException exc) {
            return 0;
        }
    }

    @Override
    public Boolean validateOtp(MyUserModel userModel) {
        if (userModel.getOtp() >= OTP_MIN_VALUE) {
            int cachedOtp = getOtp(userModel.getUsername());
            clearOtp(userModel.getUsername());
            return cachedOtp == userModel.getOtp();
        }
        return false;
    }

    @Override
    public void clearOtp(String username) {
        otpCache.invalidate(username);
    }
}
