package com.bibibiu.demo.configs;

import com.bibibiu.demo.security.MySaveAwareAuthenticationSuccessHandler;
import com.bibibiu.demo.security.RestAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

/**
 * created 3/2/19 10:04 PM
 *
 * @author arthur
 */
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    RestAuthenticationEntryPoint entryPoint;
    @Autowired
    MySaveAwareAuthenticationSuccessHandler mySuccessHandler;
    @Autowired
    SimpleUrlAuthenticationFailureHandler myFailureHandler;
    @Autowired
    SprintJwtConfig jwtConfig;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .exceptionHandling()
                    .authenticationEntryPoint(entryPoint)
                .and()
                    .authorizeRequests()
                    .antMatchers("/api/register/**","/api/auth/**").permitAll()
                    .antMatchers("/api/user/**").authenticated()
                    .antMatchers("/api/admin/**","/api/**").hasAnyAuthority("ADMIN")
                .and()
                    .formLogin()
                    .successHandler(mySuccessHandler)
                    .failureHandler(myFailureHandler)
                .and()
                    .logout().permitAll()
                .and()
                    .apply(jwtConfig);

        http.headers().frameOptions().sameOrigin();
    }

    @Bean
    public SimpleUrlAuthenticationFailureHandler failureHandler() {
        return new SimpleUrlAuthenticationFailureHandler();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
