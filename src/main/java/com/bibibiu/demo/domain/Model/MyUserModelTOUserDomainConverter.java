package com.bibibiu.demo.domain.Model;

import com.bibibiu.demo.domain.Authority;
import com.bibibiu.demo.domain.MyUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * created 3/3/19 8:09 PM
 *
 * @author arthur
 */
@Component
public class MyUserModelTOUserDomainConverter implements Converter<MyUserModel, MyUser> {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public MyUser convert(MyUserModel myUserModel) {
        List<Authority> authorities;
        String password = myUserModel.getPassword();
        if (myUserModel.getPassword() == null) {
            password = "12345";
        }
        if (myUserModel.getAuthorities() == null) {
            authorities = new ArrayList<>();
            authorities.add(Authority.builder().authority("USER").build());
        }else{
            authorities = Arrays
                    .stream(myUserModel.getAuthorities())
                    .map(s -> Authority.builder().authority(s).build()).collect(Collectors.toList());
        }
        return MyUser.builder()
                .username(myUserModel.getUsername())
                .password(passwordEncoder.encode(password))
                .authorities(authorities)
                .build();
    }
}
