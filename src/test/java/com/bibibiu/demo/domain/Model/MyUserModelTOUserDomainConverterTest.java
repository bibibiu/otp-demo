package com.bibibiu.demo.domain.Model;

import com.bibibiu.demo.domain.Authority;
import com.bibibiu.demo.domain.MyUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class MyUserModelTOUserDomainConverterTest {

    private static final String PASSWORD = "arthur";
    private static final String USERNAME = "bibibiu";
    private static final String[] ROLES = {"USER", "ADMIN"};

    @Mock
    PasswordEncoder passwordEncoder;
    @InjectMocks
    MyUserModelTOUserDomainConverter userDomainConverter;
    @Test
    public void convert() {
        //Given
        MyUserModel userModel = MyUserModel.builder()
                .username(USERNAME)
                .password(PASSWORD)
                .authorities(ROLES)
                .build();
        String encodedPassword = new BCryptPasswordEncoder().encode(PASSWORD);
        log.info("encoded password: "+ encodedPassword);

        //When
        when(passwordEncoder.encode(anyString())).thenReturn(encodedPassword);
        MyUser user = userDomainConverter.convert(userModel);

        //Then
        assert user != null;
        Authority userAuth = Authority.builder().authority("USER").build();
        Authority adminAuth = Authority.builder().authority("ADMIN").build();
        assertThat(encodedPassword, is(user.getPassword()));
        assertThat(user.getAuthorities(), hasSize(2));
        assertThat(new SimpleGrantedAuthority(userAuth.getAuthority()),
                isIn(new ArrayList<>(user.getAuthorities())));
        assertThat(new SimpleGrantedAuthority(adminAuth.getAuthority()),
                isIn(new ArrayList<>(user.getAuthorities())));
    }
}