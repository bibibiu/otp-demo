package com.bibibiu.demo.controllers;

import com.bibibiu.demo.domain.Model.MyUserModel;
import com.bibibiu.demo.security.JwtTokenProvider;
import com.bibibiu.demo.services.MyUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.ResponseEntity.ok;

/**
 * created 3/4/19 9:42 PM
 *
 * @author arthur
 */
@Slf4j
@RestController
@RequestMapping("api/auth")
public class LoginController {

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    MyUserService userService;
    @Autowired
    JwtTokenProvider tokenProvider;

    @PostMapping("login")
    public ResponseEntity login(@RequestBody MyUserModel userModel) {
        String username = userModel.getUsername();
        String password = userModel.getPassword();
        //Todo add not null assertions
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username,password));
            List<String> roles = userService.findByUsername(username).getRoles();

            String token = tokenProvider.createToken(username, roles);
            Map<Object,Object> model = new HashMap<>();
            model.put("username", username);
            model.put("token", token);
            return ok(model);
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid user name or password");
        }
    }

}
