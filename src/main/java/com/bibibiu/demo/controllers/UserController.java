package com.bibibiu.demo.controllers;

import com.bibibiu.demo.domain.Model.MyUserModel;
import com.bibibiu.demo.services.MyUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * created 3/3/19 7:41 PM
 *
 * @author arthur
 */
@Controller
@RequestMapping("api/admin")
public class UserController {
    @Autowired
    MyUserService userService;

    @PostMapping("add")
    public ResponseEntity<MyUserModel>saveUser(@RequestBody MyUserModel userModel) {
        MyUserModel savedUserModel = userService.save(userModel);

        return new ResponseEntity<>(savedUserModel, HttpStatus.OK);
    }
    @DeleteMapping("{id}/delete")
    public ResponseEntity<Void>deleteUserById(@PathVariable Long id) {
        userService.deleteById(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
