package com.bibibiu.demo.repositories;

import com.bibibiu.demo.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * created 3/3/19 12:25 AM
 *
 * @author arthur
 */
@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {
    Optional<Student> findByRegistrationNumber(String regNum);
}
