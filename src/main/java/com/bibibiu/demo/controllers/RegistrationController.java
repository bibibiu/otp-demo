package com.bibibiu.demo.controllers;

import com.bibibiu.demo.domain.Model.MyUserModel;
import com.bibibiu.demo.services.MyUserService;
import com.bibibiu.demo.services.OtpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.List;

/**
 * created 3/4/19 12:50 AM
 *
 * @author arthur
 */
@Slf4j
@Controller
@RequestMapping("api/register")
public class RegistrationController {

    @Autowired
    MyUserService userService;
    @Autowired
    OtpService otpService;

    @PostMapping
    public ResponseEntity<String>register(@RequestBody MyUserModel userModel) {
        HttpStatus status;
        String body;
        if (userModel.getUsername() == null) {
            status = HttpStatus.BAD_REQUEST;
            body = "No user name";
            return new ResponseEntity<>(body, status);
        }
        Boolean alreadyRegistered = userService.contains(userModel);

        if (alreadyRegistered) {
            status = HttpStatus.CONFLICT;
            body = "User Already exists";
        } else {
            status = HttpStatus.OK;
            body = otpService.generateOtp(userModel.getUsername()).toString();
        }

        return new ResponseEntity<>(body, status);
    }

    @PostMapping("validate")
    public ResponseEntity validateRegister(@RequestBody MyUserModel userModel) {
        Boolean isOtpValid = otpService.validateOtp(userModel);
        if (isOtpValid) {
            MyUserModel savedUserModel = userService.save(userModel);

            List<String> authorities = Arrays.asList(savedUserModel.getAuthorities());

            log.debug("roles"+ authorities.toString());

            return new ResponseEntity<>(savedUserModel, HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity<>("Invalid OTP or OTP expired", HttpStatus.UNAUTHORIZED);
        }
    }

    /*@GetMapping("me")
    public */
}
