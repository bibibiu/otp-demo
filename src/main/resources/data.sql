INSERT INTO authorities (authority)
VALUES ('ADMIN');

INSERT INTO users (username,password)
VALUES ('28680987','$2a$10$Zoz7GSVHI04l7af58mX2g.3e5dwoMArOBuY0KEOZgc2LFdp0TyO8S');

INSERT INTO user_authorities (user_id,role_id)
VALUES (1,1);

INSERT INTO students(registration_number, first_name, last_name)
VALUES ('F17-1599-2013','Arthur','Mita');
INSERT INTO students(registration_number, first_name, last_name)
VALUES ('F56-13085-2018','Arthur','Mita');
INSERT INTO students(registration_number, first_name, last_name)
VALUES ('F56-13086-2018','Lawrence','Mwaniki');

