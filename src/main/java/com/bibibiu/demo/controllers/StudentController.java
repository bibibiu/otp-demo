package com.bibibiu.demo.controllers;

import com.bibibiu.demo.domain.Student;
import com.bibibiu.demo.services.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;

/**
 * created 3/3/19 1:01 AM
 *
 * @author arthur
 */
@Slf4j
@Controller
@RequestMapping("api")
public class StudentController {

    final private StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("user/students")
    public ResponseEntity findAllStudents() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<Student> students = studentService.findAll();

        log.debug("Username: "+ auth.getName());
        log.debug("Authorities"+ auth.getAuthorities());

        return ok(students);
    }

    @GetMapping("user/students/{id}")
    public ResponseEntity findStudentById(@PathVariable Long id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Student foundStudent = studentService.findById(id);

        log.debug("Username: "+ auth.getName());
        log.debug("Authorities"+ auth.getAuthorities());

        return ok(foundStudent);
    }

    @PostMapping("admin/students")
    public ResponseEntity saveStudent (@RequestBody Student student, HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Student savedStudent = studentService.save(student);
        log.debug("Username: "+ auth.getName());
        log.debug("Authorities"+ auth.getAuthorities());
        return created(
                ServletUriComponentsBuilder
                        .fromContextPath(request).path("/api/user/students/{id}")
                        .buildAndExpand(savedStudent.getId()).toUri())
                .build();
    }
}
