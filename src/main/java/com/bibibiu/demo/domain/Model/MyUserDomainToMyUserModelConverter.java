package com.bibibiu.demo.domain.Model;

import com.bibibiu.demo.domain.MyUser;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * created 3/3/19 10:15 PM
 *
 * @author arthur
 */
@Component
public class MyUserDomainToMyUserModelConverter implements Converter<MyUser,MyUserModel> {

    @Override
    public MyUserModel convert(MyUser myUser) {
        List<GrantedAuthority> authorities = new ArrayList<>(myUser.getAuthorities());
        String[] roles = authorities.stream().map(GrantedAuthority::getAuthority).toArray(String[]::new);
        return MyUserModel.builder().authorities(roles).username(myUser.getUsername()).userId(myUser.getId()).build();
    }
}
