package com.bibibiu.demo.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * created 3/4/19 5:01 PM
 *
 * @author arthur
 */
@Slf4j
@Component
public class ControllerUtils {
    public static void securityDebug(Authentication auth) {
        log.debug("Username: "+ auth.getName());
        log.debug("Authority: "+ auth.getAuthorities());
    }
}
