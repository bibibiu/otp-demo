package com.bibibiu.demo.services;

import com.bibibiu.demo.domain.Model.MyUserModel;

public interface OtpService {
    Integer generateOtp(String username);
    Integer getOtp(String username);
    Boolean validateOtp(MyUserModel userModel);
    void clearOtp(String username);
}
