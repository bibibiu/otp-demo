package com.bibibiu.demo.domain.Model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * created 3/3/19 7:43 PM
 *
 * @author arthur
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyUserModel {
    private Long userId;
    private String username;
    private String password;
    private String[] authorities;
    private Integer otp;
    private String token;
}
