package com.bibibiu.demo.services;

import com.bibibiu.demo.domain.Model.MyUserDomainToMyUserModelConverter;
import com.bibibiu.demo.domain.Model.MyUserModel;
import com.bibibiu.demo.domain.Model.MyUserModelTOUserDomainConverter;
import com.bibibiu.demo.domain.MyUser;
import com.bibibiu.demo.repositories.MyUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

/**
 * created 3/3/19 7:31 PM
 *
 * @author arthur
 */
@Service
@Transactional
public class MyUserServiceImpl extends GenericServiceImpl<MyUser,Long> implements MyUserService {
    @Autowired
    private MyUserRepository userRepository;
    @Autowired
    private MyUserModelTOUserDomainConverter toUserDomainConverter;
    @Autowired
    private MyUserDomainToMyUserModelConverter toMyUserModelConverter;


    @Override
    public Boolean contains(MyUser object) {
        return userRepository.findByUsername(object.getUsername()).isPresent();
    }

    @Override
    public MyUserModel save(MyUserModel userModel) {
        MyUser detachedUser = toUserDomainConverter.convert(userModel);
        if (contains(detachedUser)) {
            throw new EntityExistsException("User already exists");
        }
        assert detachedUser != null;
        MyUser savedUser = userRepository.save(detachedUser);
        return toMyUserModelConverter.convert(savedUser);
    }

    @Override
    public Boolean contains(MyUserModel userModel) {
        return contains(toUserDomainConverter.convert(userModel));
    }

    @Override
    public MyUser findByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(()->
                new EntityNotFoundException("User not found"));
    }

}
