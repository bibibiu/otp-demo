package com.bibibiu.demo.domain.Model;

import com.bibibiu.demo.domain.Authority;
import com.bibibiu.demo.domain.MyUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class MyUserDomainToMyUserModelConverterTest {

    @InjectMocks
    MyUserDomainToMyUserModelConverter toMyUserModel;

    private static final Long ID = 1L;
    private static final String USERNAME = "arthur";
    private static final String ROLE1 = "ADMIN";
    private static final String ROLE2 = "USER";

    @Test
    public void convert() {
        //Given
        MyUser user = MyUser.builder()
                .id(1L)
                .username("arthur")
                .authority(Authority.builder().authority(ROLE1).build())
                .authority(Authority.builder().authority(ROLE2).build())
                .build();
        //When
        MyUserModel converted = toMyUserModel.convert(user);
        //Then
        assert converted != null;
        assertThat(ID, is(converted.getUserId()));
        assertThat(new String[]{ROLE1,ROLE2}, is(converted.getAuthorities()));
        assertThat(USERNAME, is(converted.getUsername()));

    }
}