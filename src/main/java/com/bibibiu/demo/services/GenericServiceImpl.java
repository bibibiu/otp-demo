package com.bibibiu.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * created 3/3/19 12:51 AM
 *
 * @author arthur
 */
@SuppressWarnings({"SpringJavaAutowiredFieldsWarningInspection", "SpringJavaInjectionPointsAutowiringInspection"})
@Service
public abstract class GenericServiceImpl<S, ID extends Serializable> implements GenericService<S, ID> {


    @Autowired
    private JpaRepository<S, ID> repository;

    @Override
    public List<S> findAll() {
        return repository.findAll();
    }

    @Override
    public S findById(ID id) {
        return repository.findById(id).orElseThrow(()->new RuntimeException("Not found"));
    }

    @Override
    public S save(S object) {
        return repository.save(object);
    }

    @Override
    public void deleteById(ID id) {
        repository.deleteById(id);
    }

    public abstract Boolean contains(S object);

}
