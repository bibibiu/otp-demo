package com.bibibiu.demo.services;

import java.io.Serializable;
import java.util.List;

public interface GenericService<S, ID extends Serializable> {
    List<S> findAll();
    S  findById(ID id);
    S save(S object);
    void deleteById(ID id);
    Boolean contains(S object);
}
